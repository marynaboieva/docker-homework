var express = require('express');
var path=require("path");
var router = express.Router();
const data = require('../resources/api/fighters.json')

/* GET home page. */

router.get('/', function(req, res, next) {
  res.json(data);
});

module.exports = router;
