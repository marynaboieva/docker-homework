var express = require('express');
var router = express.Router();
var { saveData,getData,getAll,updateData} = require("../services/user.service");

router.get('/', function(req, res, next) {
  res.render('../users.pug',users=getAll());
});

router.post('/', function(req, res, next) {
  var result=null;
  if(req.body.name!=""&&req.body.pass!=""){
    var user={
      'name':req.body.name,
      'pass':req.body.pass,
      'fights':'0'
    }
    result = saveData(user);
    if (result!=null) {
      req.session.name=req.body.name;
      res.redirect("/user/"+req.body.name);
    }
  }
  else {
    res.status(400).send(`Some error`);
  }
});
router.get('/:name',function(req,res,next){
  var user=getData(req.params.name);
  if(user!=null) {
    if(req.params.name==req.session.name)  res.render('../user.pug',{user:user,isAuth:true});
    else  res.render('../user.pug',{user});
  }else res.status(404).send("We don`t have a user "+req.params.name);
});
router.put('/:name',function(req,res,next){
  if(req.params.name){
    if(req.params.name==req.session.name){
      updateData(req.session.name);
    }
    else res.send("You have to be loged in to do this");
  }else {
    res.status(400).send(`Some error`);
  }
});
module.exports = router;
