var express = require('express');
var path=require('path');
var router = express.Router();
var { getData } = require("../services/user.service");
router.post('/', function(req, res, next){
    var data=getData(req.body.name);
    if(data){
        if(data.pass==req.body.pass){
            req.session.name=req.body.name;
            res.redirect("../");
        }
        else res.send('Wrong password');
      }
      else if(data==null) res.redirect(307,"../user/");
});
router.get('/',function(req,res,next){
    res.sendFile(path.resolve(__dirname+'./../public/login.html'));
});
module.exports=router;