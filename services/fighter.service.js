var fightersDetails = require("../repositories/fighter.repository");
var getData=function(name){
    if(name){
        return fightersDetails.filter(function(elem){
            return name===elem.name;
        })[0];
    }
    else return null;
}
module.exports={getData};