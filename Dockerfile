FROM node:6-alpine

COPY dist ./dist
COPY bin ./bin
COPY public ./public
COPY resources ./resources
COPY repositories ./repositories
COPY routes ./routes
COPY services ./services
COPY app.js ./
COPY fighter.pug ./
COPY user.pug ./
COPY users.pug ./
COPY package*.json ./
RUN npm install

CMD ["node", "./bin/www"]
EXPOSE 3000