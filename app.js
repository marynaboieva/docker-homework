var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/user');
var fightersRouter =require('./routes/fighters');
var fighterRouter=require('./routes/fighter');
var loginRouter=require('./routes/login');
var logoutRouter=require('./routes/logout');
var app = express();
app.disable('etag');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session( {
    secret: 'wount tell ya',
    resave: true,
    saveUninitialized: false
  }));
app.use(express.static(__dirname));
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/fighters',fightersRouter);
app.use('/fighter',fighterRouter);
app.use('/login',loginRouter);
app.use('/logout',logoutRouter);
module.exports = app;