var fs=require('fs');
var path=require("path");
var arr=[];
var saveUserData = function(data){
  if(data){
   fs.readFile(path.resolve(__dirname+'./../resources/api/users.json'), 'utf8', function readFileCallback(err, text){
      if (err){
        console.log(err);
      } else {
        if(text){
          arr = JSON.parse(text); //now it an arrect
        }
        arr.push(data); //add some data
        json = JSON.stringify(arr); //convert it back to json
       fs.writeFileSync(path.resolve(__dirname+'./../resources/api/users.json'), json, 'utf8'); // write it back 
    }});
    return true;
  } else {
    return false;
  }
}
var getUserData=function(){
  var file=fs.readFileSync(path.resolve(__dirname+'./../resources/api/users.json'),'utf8');
  if(file!="") return JSON.parse(file);
  else return null
} 
var updateUserData=function(name){
  var file=JSON.parse(fs.readFileSync(path.resolve(__dirname+'./../resources/api/users.json'),'utf8'));
  file.forEach(function(elem){
    if(elem.name==name) elem.fights=parseInt(elem.fights)+1;
  });
  fs.writeFileSync(path.resolve(__dirname+'./../resources/api/users.json'), JSON.stringify(file), 'utf8');
  return true;
}
module.exports = {
  saveUserData,getUserData,updateUserData
};